.NET Core Sample Application using docker
===

Debugging
---

### Introduction

The purpose of this repository is to show how to develop and publish a .NET Core application without having to install the dotnet sdk. This sample depends solely on docker containers to eliminate as many dependencies as possible to how your workstation is installed. This example uses an MVC application, but there is no reason why it should not also work for other .NET applications.

This repository focusses on a Watcher. The idea is that the .NET Core application gets rebuild whenever the source code changes. This would be easily achieved by simply installing the dotnet sdk and starting `dotnet watch run` in the directory that contains the source code for the application. What the samlpe in this repository adds, is that the watch process runs in a container and that it has debugging support.

So whenever you make changes to your source code the application gets rebuild and you immediately see your changes reflected. You could also have a test project where the tests are rerun every time you make changes to either the source code or the test project. This is not demonstrated in this repository. The [dotnet-docker repository](https://github.com/dotnet/dotnet-docker/) has a very good example on how to achieve that.

I've spent quite some time on figuring out how to get debugging working in a container that uses `dotnet watch`. After a long time I found a working solution by Dispersia on [reddit](https://www.reddit.com/r/csharp/comments/9cyl6a/debugging_dotnet_watch_processes_inside_of_a/). I modified it so that it also works when the .NET Core application is not in the root of the repository, so that you could have one directory for the .NET Core application and another one for its unit tests, for example.

### The Watch-container

Please have a look at the `Dockerfile` for the dotnet watcher.

The Watch container starts from the dotnet-sdk base image.

The thing that makes debugging possible is the `vsdbg` application. It turns out that Microsoft provides a script for downloading and installing it in a similar way that you can install `docker` for example. All it needs to function is `unzip`, so it installs that first.

Also notice that it defines the environment variable `DOTNET_USE_POLLING_FILE_WATCHER`. We will use that later to do different things when we're inside a container than when we're running locally.

This container expects applications to be copied or mounted to `/app`.

The last thing this container does by default, is execute `dotnet watch run` with the extra argument `--urls=http://+:5000`. This is necessary because by default .NET Core Web Applications only listen to `localhost`. But if you're running the web application inside a container and bind a port on the host to a port in the container, then to the container the request will not come from `localhost` but from another IP address and the web application will not service the request.

(If the `--urls` argument is troublesome for .NET Core applications that are not .NET Core Web Applications, then perhaps that argument should be moved to a `COMMAND` or be absent alltogether and documented somewhere).

The subdirectory `watcher` also contains a `Makefile` that allows you to build the container by executing `make build` from inside that directory. In this case we want it to use different directories when running in the container than when running locally.

### MSBuild configuration

The container is built using `MSBuild`. There is some configuration that you can do on how it should build like what directories to use for example by adding the file `Directory.Build.props` to the root of the .NET Core application.

The new configuration is that if the environment variable `DOTNET_RUNNING_IN_CONTAINER` is set (remember that this variable is set in the watcher's `Dockerfile`) `obj` and `bin` files are stored in the subdirectory `container` and if the .NET Core Application is run locally it is stored in the subdirectory `local`.

### Running the watch-container

For running the watch-container I created a `docker-compose.yml`. It names the container "dnx", which is important later. It uses the `ghanique/dotnetwatcher` image. (Not sure anymore why the `entrypoint` is there, too).

By default the dotnet-sdk image expects the .NET Core application to be in the directory `/app`, which is also the working directory. What is most relevant in this `docker-compose` file is that the `working_dir` is set to the `dnx` subdirectory and that also becomes the new working dir. This is important for how to configure debugging in VSCode.

### .vscode/launch.json

For debugging in the watch container to work there must be a configuration that tells VSCode how to connect to the debugger and what process to debug.

In order to connect inside the container, we define a `pipeTransport`: `docker exec -i dnx`, where "dnx" is the name of the container running the watcher. (So this has to match the `container_name` in `docker-compose.yml`). Once inside the container VSCode can run the debugger at `/vsdbg/vsdbg`.

Now the somewhat confusing part is: we don't know the `processId` of the process that must be debugged, because it's not VSCode that starts the process to debug, but the watcher. That's why the configuration uses `${command:pickRemoteProcess}`.

### Debugging the web page

First start the watcher by executing `docker-compose up`.

In the browser let's first go to [http://localhost:5000](http://localhost:5000) to see if the website is working.

Now to debug you can set a breakpoint anywhere in the code. I suggest somewhere in the `Index` action of the `HomeController`. Then press F5 to start debugging. You'll get a dropdown list of processes running inside the container. Select the process that contains "dnx.dll", or whatever your application is called. (`dotnet exec /app/dnx/bin/container/dnx.dll --urls=http://+:5000`). Now go to the browser and open or refresh the page that triggers the break-point. Notice that you can see all the variables, continue to the next line, etc.

When you're done click the small button for "disconnect".

### Conclusion

Whatever change you make to the source code, it will automatically be reflected in the web application. (In the browser you may have to refresh the page). If you want to debug, just press F5, select the process with the dll and you'll be debugging the web application that you just updated.

You shouldn't change the source code while you're debugging, though, because the watcher won't be able to build the web application, because the dll is locked by the debugger.

Building a release
---

When you want to release a web application you have to build a container that contains it and that can run it. Before you can run it you first have to build it.

You can only build a .NET Core Web Application in the `dotnet-sdk` image. Problem with that one is that it's a rather large image: about 1.75 GB in size. It would be better if you could run it in the `aspnetcore-runtime` image, wihich is "only" 235 MB in size. Fortunately docker support multi-stage builds.

The `Dockerfile` in the root of this project starts from the `dotnet-sdk` image. First it copies the project files to the image and executes `dotnet restore` to download and install all packages that are used by this project. Then it copies the rest and builds the application.

The reason why building the application is done in two stages, is because the csproj-files don't change that often. If they don't change then the step that restores the packages, which takes a relative long time, is skipped.

After building the application we create a new docker image from the `aspnetcore-runtime` base image and copy the application that we build in the `dotnet-sdk` image into the `runtime` image and that will be our new application.

To make this easier the root of this repository also contains a `Makefile` to build and run the application image.
