FROM microsoft/dotnet:2.1-sdk AS build
WORKDIR /app

# copy csproj and restore as distinct layers
COPY dnx/*.csproj ./
RUN dotnet restore

# copy everything else and build app
COPY dnx/ .
RUN dotnet build

# publish the app
FROM build AS publish
RUN dotnet publish -c Release -o out

FROM microsoft/dotnet:2.1-aspnetcore-runtime AS runtime
WORKDIR /app
COPY --from=publish /app/out ./
ENTRYPOINT ["dotnet", "dnx.dll"]
