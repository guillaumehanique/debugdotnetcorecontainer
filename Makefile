MAINTAINER=ghanique
IMAGENAME=dnx
VERSION=0.1.0

.PHONY: help
help:
	@echo "Please use any of the following targets:"
	@echo "    build        Build the dockerfile. Tag it with '${MAINTAINER}/${IMAGENAME}'."
	@echo "    run          Run the image '${MAINTAINER}/${IMAGENAME}' in a container."
	@echo "    watch        Run the watch-container that rebuilds the website"
	@echo "                 every time you make a change."

.PHONY: build
build:
	docker build -t ${MAINTAINER}/${IMAGENAME} -t ${MAINTAINER}/${IMAGENAME}:${VERSION} .

.PHONY: run
run:
	docker run --rm -it -p 5000:80 ${MAINTAINER}/${IMAGENAME}:${VERSION}

.PHONY: watch
watch:
	docker-compose up
